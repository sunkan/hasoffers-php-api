<?php

namespace HasOffers\Brand;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class ApiKey extends AService
{
    public function findApiKeys(array $filters = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters'] = $filters;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\StaticApiKey($row);
        }

        return $rows;
    }
    public function generateApiKey($user_type, $user_id)
    {
        if (!in_array($user_type, ['affiliate'])) {
            throw new InvalidArgumentException('invalid user_type');
        }
        if (!is_int($user_id) || $user_id < 1) {
            throw new InvalidArgumentException('user_id must be an integer and be positive');
        }
        $payload = [];
        $payload['user_type'] = $user_type;
        $payload['user_id'] = $user_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\StaticApiKey($response['data']);
    }
    public function getUserApiKey($user_type, $user_id)
    {
        if (!in_array($user_type, ['affiliate'])) {
            throw new InvalidArgumentException('invalid user_type');
        }
        if (!is_int($user_id) || $user_id < 1) {
            throw new InvalidArgumentException('user_id must be an integer and be positive');
        }
        $payload = [];
        $payload['user_type'] = $user_type;
        $payload['user_id'] = $user_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'] ? new Model\StaticApiKey($response['data']) : null;
    }
    public function regenerateApiKey($user_type, $user_id, $existing_api_key)
    {
        if (!in_array($user_type, ['affiliate'])) {
            throw new InvalidArgumentException('invalid user_type');
        }
        if (!is_int($user_id) || $user_id < 1) {
            throw new InvalidArgumentException('user_id must be an integer and be positive');
        }
        $payload = [];
        $payload['user_type'] = $user_type;
        $payload['user_id'] = $user_id;
        $payload['existing_api_key'] = $existing_api_key;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\StaticApiKey($response['data']);
    }
    public function updateApiKeyStatus($key_id, $status)
    {
        if (!in_array($status, ['pending', 'rejected', 'active'])) {
            throw new InvalidArgumentException('invalid status');
        }
        if (!is_int($key_id) || $key_id < 1) {
            throw new InvalidArgumentException('key_id must be an integer and be positive');
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
