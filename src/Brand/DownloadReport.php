<?php

namespace HasOffers\Brand;

use HasOffers\AService;
use InvalidArgumentException;

class DownloadReport extends AService
{
    public function getDownloadReportLink($method, $arguments, $start_date, $end_date, $user_type = false)
    {
        if (!in_array($method, ['getStats', 'getConversions', 'getAffiliateCommissions', 'getReferrals', 'getModSummaryLogs', 'getSubscriptions'])) {
            throw new InvalidArgumentException('not a valida method');
        }

        $start_date = $this->_getDate($start_date);
        $end_date = $this->_getDate($end_date);

        $payload = [];

        $payload['method'] = $method;
        $payload['start_date'] = $start_date->format('Y-m-d');
        $payload['end_date'] = $end_date->format('Y-m-d');
        $payload['arguments'] = $arguments;
        if ($user_type) {
            $payload['user_type'] = $user_type;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
