<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class CustomMenuLink extends AService
{
    public function find()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findActive()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
