<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class AdvertiserUser extends AService
{
    protected $target = 'AdvertiserUser';

    public function checkPassword($id, $password)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function create($data, $return_object = true)
    {
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
    }
    public function findAll(array $filters, array $sort = [], $limit = 25, $page = 1, array $fields = [], array $contain = [])
    {
    }
    public function findAllByIds(array $ids, array $fields = [], array $contain = [])
    {
    }
    public function findAllIds()
    {
    }
    public function findAllIdsByAdvertiserId($advertiser_id)
    {
        if (!is_int($advertiser_id) || $advertiser_id < 1) {
            throw new InvalidArgumentException('advertiser_id must be an integer and be positive');
        }
    }
    public function findById($id, array $fields = [], array $contain = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function grantAccess($id, $permission)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function removeAccess($id, $permission)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function resetPassword($id, $length = 8)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function uniqueEmail($email)
    {
    }
    public function update($id, $data, $return_object = true)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
    }
    public function updateField($id, $field, $value, $return_object = false)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
    }
}
