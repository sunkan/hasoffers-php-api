<?php

namespace HasOffers\Brand;

use HasOffers\AService;
use InvalidArgumentException;

class RingRevenue extends AService
{
    public function findAll(array $fields = [], array $filters = [], array $sort = [], $limit = false, $page = false)
    {
        $payload = [];

        if ($limit && !is_int($limit)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        if ($page && !is_int($page)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }

        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($page) {
            $payload['page'] = $page;
        }

        if (count($fields)) {
            $payload['fields'] = $fields;
        }
        if (count($filters)) {
            $payload['filters'] = $filters;
        }
        if (count($sort)) {
            $payload['sort'] = $sort;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
