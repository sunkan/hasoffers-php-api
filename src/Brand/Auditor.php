<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class Auditor extends AService
{
    protected $target = 'Auditor';
    public function findBrandDataAudits()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
