<?php

namespace HasOffers\Brand;

use HasOffers\AService;
use InvalidArgumentException;

class Notification extends AService
{
    public function clearUserSubscriptions($user_id)
    {
        if (!is_int($user_id) || $user_id < 1) {
            throw new InvalidArgumentException('user_id must be an integer and be positive');
        }

        $payload = [];
        $payload['user_id'] = $user_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getUserSubscriptions($user_id, array $filter = [])
    {
        if (!is_int($user_id) || $user_id < 1) {
            throw new InvalidArgumentException('user_id must be an integer and be positive');
        }

        $payload = [];
        $payload['user_id'] = $user_id;
        $payload['filter'] = $filter;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function replaceUserSubscriptions($user_id, array $scenario_ids)
    {
        if (!is_int($user_id) || $user_id < 1) {
            throw new InvalidArgumentException('user_id must be an integer and be positive');
        }

        $payload = [];
        $payload['user_id'] = $user_id;
        $payload['scenario_ids'] = $scenario_ids;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
