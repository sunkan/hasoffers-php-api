<?php

namespace HasOffers\Brand;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class AdManager extends AService
{
    protected $target = 'AdManager';

    public function addCreative($campaign_id, $data)
    {
        if (!is_int($campaign_id)) {
            throw new InvalidArgumentException('campaign_id must be an integer');
        }
        if (!($data instanceof Model\AdCampaignCreative)) {
            $data = new Model\AdCampaignCreative($data);
        }

        $this->makeRequest('addCreative', [
            'campaign_id' => $campaign_id,
            'data' => $data,
        ]);
    }
    public function createCampaign($data, $return_object = true)
    {
        if (!($data instanceof Model\AdCampaign)) {
            $data = new Model\AdCampaign($data);
        }

        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }

        $this->makeRequest('createCampaign', [
            'return_object' => $return_object,
            'data' => $data,
        ]);
    }
    public function findAllCampaigns(array $filters, array $sort = [], $limit = 25, $page = 1, array $fields = [], array $contain = [])
    {
        if (!is_int($page) || $page < 1) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if (!is_int($limit) || $limit < 1) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [
            'filters' => $filters,
            'limit' => $limit,
            'page' => $page,
        ];
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $this->makeRequest('findAllCampaigns', $payload);
    }
    public function findAllCreatives(array $filters, array $sort = [], $limit = 25, $page = 1, array $fields = [], array $contain = [])
    {
        if (!is_int($page) || $page < 1) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if (!is_int($limit) || $limit < 1) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [
            'filters' => $filters,
            'limit' => $limit,
            'page' => $page,
        ];
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $this->makeRequest('findAllCreatives', $payload);
    }
    public function findCampaignById($id, array $fields = [], array $contain = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [
            'id' => $id,
        ];
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $this->makeRequest('findCampaignById', $payload);
    }
    public function findCreativeById($id, array $fields = [], array $contain = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [
            'id' => $id,
        ];
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $this->makeRequest('findCreativeById', $payload);
    }
    public function getActiveNetworkCampaignCount($affiliate_access)
    {
        if (!is_int($affiliate_access) || $affiliate_access < 1) {
            throw new InvalidArgumentException('affiliate_access must be an integer and be positive');
        }
        $this->makeRequest('findCreativeById', [
            'affiliate_access' => $affiliate_access,
        ]);
    }
    public function getCampaignCode($campaign_id, $affiliate_id, array $params = [], array $options = [])
    {
        if (!is_int($campaign_id) || $campaign_id < 1) {
            throw new InvalidArgumentException('campaign_id must be an integer and be positive');
        }
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        $payload = [
            'campaign_id' => $campaign_id,
            'affiliate_id' => $affiliate_id,
        ];

        if (count($params)) {
            $payload['params'] = $params;
        }

        $cleanOptions = [];
        if (count($options)) {
            $allowed = [
                'format' => ['fbiframe', 'iframe', 'javascript', 'url'],
                'eredirect' => true,
                'redirect' => true,
            ];
            foreach ($options as $key => $value) {
                if (isset($allowed[$key])) {
                    if ($allowed[$key] === true || (is_array($allowed[$key]) && in_array($value, $allowed[$key]))) {
                        $cleanOptions[$key] = $value;
                    }
                }
            }
        }
        if (count($cleanOptions)) {
            $payload['options'] = $cleanOptions;
        }

        $this->makeRequest('getCampaignCode', $payload);
    }
    public function getCampaignCreatives($id, array $filters, array $sort = [], $limit = 25, $page = 1, array $fields = [], array $contain = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_int($page) || $page < 1) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if (!is_int($limit) || $limit < 1) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [
            'id' => $id,
            'filters' => $filters,
            'limit' => $limit,
            'page' => $page,
        ];
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $this->makeRequest('getCampaignCreatives', $payload);
    }
    public function getUsage($start_date = false, $end_date = false)
    {
        $payload = [];
        if ($start_date !== false) {
            $start_date = $this->_getDate($start_date);
            $payload['start_date'] = $start_date->format('Y-m-d');
        }
        if ($end_date !== false) {
            $end_date = $this->_getDate($end_date);
            $payload['end_date'] = $end_date->format('Y-m-d');
        }
        $this->makeRequest('getUsage', $payload);
    }
    /**
     * key = campaign_id
     * value = weight
     * $data = [
     *   1=>12
     * ].
     */
    public function setCreativeCustomWeights($data)
    {
        $this->makeRequest('setCreativeCustomWeights', $data);
    }
    public function setCreativeWeights($data)
    {
        $this->makeRequest('setCreativeCustomWeights', $data);
    }
    public function updateCampaign($id, $data, $return_object = true)
    {
        if (!($data instanceof Model\AdCampaign)) {
            $data = new Model\AdCampaign($data);
        }

        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }

        $this->makeRequest('updateCampaign', [
            'id' => $id,
            'return_object' => $return_object,
            'data' => $data,
        ]);
    }
    public function updateCampaignField($id, $field, $value, $return_object = false)
    {
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
        $this->makeRequest('updateCampaign', [
            'id' => $id,
            'field' => $field,
            'value' => $value,
            'return_object' => $return_object,
        ]);
    }
    public function updateCreative($id, $data, $return_object = true)
    {
        if (!($data instanceof Model\AdCampaignCreative)) {
            $data = new Model\AdCampaignCreative($data);
        }

        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }

        $this->makeRequest('updateCreative', [
            'id' => $id,
            'return_object' => $return_object,
            'data' => $data,
        ]);
    }
    public function updateCreativeField($id, $field, $value, $return_object = false)
    {
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
        $this->makeRequest('updateCampaign', [
            'id' => $id,
            'field' => $field,
            'value' => $value,
            'return_object' => $return_object,
        ]);
    }
}
