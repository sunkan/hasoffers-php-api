<?php

namespace HasOffers\Brand;

abstract class AService extends \HasOffers\AService
{
    protected function makeRequest($method, $data, $requestMethod = false)
    {
        $data['NetworkToken'] = $this->options['NetworkToken'] ?: $this->options['network_token'];

        return parent::makeRequest($method, $data, $requestMethod);
    }
}
