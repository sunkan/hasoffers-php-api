<?php

namespace HasOffers\Brand;

use HasOffers\Model;
use InvalidArgumentException;

class AffiliateBilling extends AService
{
    public function addInvoiceItem()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createInvoice()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createReceipt()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllInvoices()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllInvoicesByIds()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllReceipts()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllReceiptsByIds()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findInvoiceById()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['data'];
    }
    public function findInvoiceStats()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['data'];
    }
    public function findLastInvoice($affiliate_id)
    {
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;

        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return new Model\AffiliateInvoice($response['AffiliateInvoice']);
    }
    public function findLastReceipt()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['data'];
    }
    public function findReceiptById()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['data'];
    }
    public function generateInvoices()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getAccountBalance($affiliateId)
    {
        $payload = [
            'affiliateId' => $affiliateId,
        ];

        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['data'];
    }
    public function getAccountHistory($affiliate_id)
    {
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $response = $this->makeRequest(__FUNCTION__, $payload);
        $rows = [
            'invoices' => [],
            'receipts' => [],
        ];
        if (count($response)) {
            foreach ($response as $row) {
                if (isset($row['AffiliateInvoice'])) {
                    $rows['invoices'][] = new Model\AffiliateInvoice($row['AffiliateInvoice']);
                } elseif (isset($row['AffiliateReceipt'])) {
                    $rows['receipts'][] = new Model\AffiliateReceipt($row['AffiliateReceipt']);
                }
            }
        }

        return $rows;
    }
    public function getNextStartDate($affiliate_id, $currency = false)
    {
        $payload = [
            'affiliate_id' => $affiliate_id,
        ];
        if ($currency && strlen($currency) == 3) {
            $payload['currency'] = $currency;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response;
    }
    public function getOutstandingInvoices()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['data'];
    }
    public function getPayoutTotals()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['data'];
    }
    public function removeInvoiceItem()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateInvoice()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateInvoiceField()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateReceipt()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateReceiptField()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateTaxInfo($id, array $data)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response;
    }
}
