<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class Advertiser extends AService
{
    protected $target = 'Advertiser';

    public function addAccountNote($id, $note)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function block($id, $reason)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function blockAffiliate($id, $affiliate_id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
    }
    public function create($data, $return_object = true)
    {
    }
    public function createSignupQuestion($data)
    {
    }
    public function createSignupQuestionAnswer($id, $data)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }

    public function findAll(array $filters, array $sort = [], $limit = 25, $page = 1, array $fields = [], array $contain = [])
    {
    }
    public function findAllByIds(array $ids, array $fields = [], array $contain = [])
    {
    }
    public function findAllIds(array $filters = [], $limit = 25, $page = 1)
    {
    }
    public function findAllIdsByAccountManagerId($employee_id)
    {
        if (!is_int($employee_id) || $employee_id < 1) {
            throw new InvalidArgumentException('employee_id must be an integer and be positive');
        }
    }
    public function findAllPendingUnassignedAdvertiserIds($manager_id)
    {
        if (!is_int($manager_id) || $manager_id < 1) {
            throw new InvalidArgumentException('manager_id must be an integer and be positive');
        }
    }
    public function findAllPendingUnassignedAdvertisers($employee_id)
    {
        if (!is_int($employee_id) || $employee_id < 1) {
            throw new InvalidArgumentException('employee_id must be an integer and be positive');
        }
    }
    public function findById($id, array $fields = [], array $contain = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getAccountBalance($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getAccountManager($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getAccountNotes($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getBlockedAffiliateIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getBlockedReasons($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getCreatorUser($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getOverview(array $advertiser_ids = [], array $filters = [], $employee_id = false)
    {
    }
    public function getOwnersAdvertiserAccountId()
    {
    }
    public function getSignupAnswers($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function getSignupQuestions($status = false)
    {
    }
    public function getUnblockedAffiliateIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function signup($account, $user, $meta, $return_object = true)
    {
    }
    public function unblockAffiliate()
    {
    }
    public function update($id, $affiliate_id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
    }
    public function updateAccountNote($account_note_id, $note)
    {
        if (!is_int($account_note_id) || $account_note_id < 1) {
            throw new InvalidArgumentException('account_note_id must be an integer and be positive');
        }
    }
    public function updateField($id, $field, $value, $return_object = false)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
    }
    public function updateSignupQuestion($question_id, $data)
    {
        if (!is_int($question_id) || $question_id < 1) {
            throw new InvalidArgumentException('question_id must be an integer and be positive');
        }
    }
    public function updateSignupQuestionAnswer($answer_id, $data)
    {
        if (!is_int($answer_id) || $answer_id < 1) {
            throw new InvalidArgumentException('answer_id must be an integer and be positive');
        }
    }
}
