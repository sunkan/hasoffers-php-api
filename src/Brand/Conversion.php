<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class Conversion extends AService
{
    public function create()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAddedConversions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAll()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllByIds()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findById()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findUpdatedConversions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function update()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateField()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateMeta()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateStatus()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
