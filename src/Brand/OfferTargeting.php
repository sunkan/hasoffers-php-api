<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class OfferTargeting extends AService
{
    public function addTargetRuleToOffer()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createTargetRule()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function deleteTargetRule()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findTargetRules()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getActiveOfferIdsUsingRule()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getActiveUsesOfRule()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getRuleTargetingForOffer()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function removeTargetRuleFromOffer()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateTargetRule()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateTargetRuleOfferAction()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
