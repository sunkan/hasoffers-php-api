<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class Authentication extends AService
{
    public function findUserByCredentials()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findUserByToken()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
