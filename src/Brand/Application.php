<?php

namespace HasOffers\Brand;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class Application extends AService
{
    public function addAffiliateTier($data)
    {
        if (!($data instanceof Model\AffiliateTier)) {
            $data = new Model\AffiliateTier($data);
        }
        $payload = [];
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function addHostname($data)
    {
        if (!($data instanceof Model\Hostname)) {
            $data = new Model\Hostname($data);
        }
        $payload = [];
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function addOfferCategory($data)
    {
        if (!($data instanceof Model\OfferCategory)) {
            $data = new Model\OfferCategory($data);
        }
        $payload = [];
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function addOfferGroup($data)
    {
        if (!($data instanceof Model\OfferGroup)) {
            $data = new Model\OfferGroup($data);
        }
        $payload = [];
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function changeAdvertiserApiKey($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function changeAffiliateApiKey($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function changeNetworkApiKey()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createAdvertiserApiKey($data)
    {
        if (!($data instanceof Model\AdvertiserApiKey)) {
            $data = new Model\AdvertiserApiKey($data);
        }
        $payload = [];
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createAffiliateApiKey($data)
    {
        if (!($data instanceof Model\AffiliateApiKey)) {
            $data = new Model\AffiliateApiKey($data);
        }
        $payload = [];
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function decryptUnsubHash($hash)
    {
        $payload = [];
        $payload['hash'] = $hash;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAdvertiserApiKey($api_key)
    {
        $payload = [];
        $payload['api_key'] = $api_key;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\AdvertiserApiKey($response['data']);
    }
    public function findAdvertiserApiKeyByAdvertiserId($advertiser_id)
    {
        $payload = [];
        $payload['advertiser_id'] = $advertiser_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AdvertiserApiKey($row);
        }

        return $rows;
    }
    public function findAffiliateApiKey($api_key)
    {
        $payload = [];
        $payload['api_key'] = $api_key;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\AffiliateApiKey($response['data']);
    }
    public function findAffiliateApiKeyByAffiliateId($affiliate_id)
    {
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AffiliateApiKey($row);
        }

        return $rows;
    }
    public function findAllAdvertiserApiKeys(array $filters = [], array $fields = [], array $contain = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters̈́'] = $filters;
        }
        if (count($fields)) {
            $payload['fields'] = $fields;
        }
        if (count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AdvertiserApiKey($row);
        }

        return $rows;
    }
    public function findAllAffiliateApiKeys(array $filters = [], array $fields = [], array $contain = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters̈́'] = $filters;
        }
        if (count($fields)) {
            $payload['fields'] = $fields;
        }
        if (count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AffiliateApiKey($row);
        }

        return $rows;
    }
    public function findAllAffiliateTierAffiliateIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllAffiliateTiers(array $filters = [], array $fields = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters̈́'] = $filters;
        }
        if (count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AffiliateTier($row);
        }

        return $rows;
    }
    public function findAllBrowsers()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\Browser($row);
        }

        return $rows;
    }
    public function findAllCountries()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\Country($row);
        }

        return $rows;
    }
    public function findAllHostnames(array $filters = [], array $fields = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters̈́'] = $filters;
        }
        if (count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\Hostname($row);
        }

        return $rows;
    }
    public function findAllOfferCategories(array $filters = [], array $fields = [], array $sort = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters̈́'] = $filters;
        }
        if (count($fields)) {
            $payload['fields'] = $fields;
        }
        if (count($sort)) {
            $payload['sort'] = $sort;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\OfferCategory($row);
        }

        return $rows;
    }
    public function findAllOfferCategoryOfferIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllOfferGroupOfferIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllOfferGroups(array $filters = [], array $fields = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters̈́'] = $filters;
        }
        if (count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\OfferGroup($row);
        }

        return $rows;
    }
    public function findAllPermissions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\Permission($row);
        }

        return $rows;
    }
    public function findAllRegions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\Region($row);
        }

        return $rows;
    }
    public function findAllTimezones()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\Timezone($row);
        }

        return $rows;
    }
    public function findBrowserById($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\Browser($response['data']);
    }
    public function findCountryByCode($code)
    {
        $length = strlen($code);
        if ($length < 2 || $length > 3) {
            throw new InvalidArgumentException('code is 2 or 3 letters');
        }
        $payload = [];
        $payload['code'] = $code;
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\Country($response['data']);
    }
    public function findPermissionById($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findPermissionByName($name)
    {
        $payload = [];
        $payload['name'] = $name;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findPermissionsByGroup($group)
    {
        $payload = [];
        $payload['group'] = $group;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findTimezoneById($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findUserAuthIps(array $filters = [], array $sort = [], $limit = false, $page = false, array $fields = [])
    {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limt && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = $page;
        }
        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\UserIp($row);
        }

        return $rows;
    }
    public function generateAllUnsubLinks($users)
    {
        $rUsers = [];
        foreach ($users as $user) {
            if (!isset($user['preference'], $user['type'], $user['user_id'])) {
                throw new DomainException('Not a valid user. Needs preference, type and user_id');
            }
            if (!in_array($user['preference'], ['disable_mail_room_email', 'disable_hasoffers_news_email'])) {
                throw new InvalidArgumentException('invalid preference');
            }
            if (!in_array($user['type'], ['aff', 'adv', 'emp'])) {
                throw new InvalidArgumentException('invalid type');
            }
            if (!is_int($user['user_id']) || $user['user_id'] < 1) {
                throw new InvalidArgumentException('id must be an integer and be positive');
            }
            $rUsers[] = $user;
        }

        $payload = [];
        $payload['users'] = $rUsers;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function generateUnsubLink($preference, $type, $user_id, $ttl)
    {
        if (!in_array($preference, ['disable_company_news_email', 'disable_offer_updates_email', 'disable_mail_room_email', 'disable_hasoffers_education_email', 'disable_hasoffers_updates_email', 'disable_hasoffers_news_email'])) {
            throw new InvalidArgumentException('invalid preference');
        }
        if (!in_array($type, ['aff', 'adv', 'emp'])) {
            throw new InvalidArgumentException('invalid type');
        }
        if (!is_int($user_id) || $user['user_id'] < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getAccountInformation()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getActiveOfferCategoryCount()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getBrand()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\BrandInformation($response['data']);
    }
    public function getBrandInformation()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\BrandInformation($response['data']);
    }
    public function getCountryRegions($code)
    {
        $length = strlen($code);
        if ($length < 2 || $length > 3) {
            throw new InvalidArgumentException('code is 2 or 3 letters');
        }
        $payload = [];
        $payload['code'];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getPoFile($language)
    {
        $length = strlen($language);
        if ($length < 2 || $length > 3) {
            throw new InvalidArgumentException('language is 2 or 3 letters');
        }
        $payload = [];
        $payload['language'] = $language;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getTimezone()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    // @todo
    public function importAdvertisers()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    // @todo
    public function importOffers()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function resetPassword($email)
    {
        $payload = [];
        $payload['email'] = $email;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateAccountInformation()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateAdvertiserApiKey()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateAffiliateApiKey()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateAffiliateTier()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateBrandEmail()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateBrandHostname()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateBrandJumpHostname()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateBrandJumpHostnameHasSsl()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateBrandNetworkName()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateHostname()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateOfferCategory()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateOfferGroup()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function uploadPoFile()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function validAdvertiserApiKey()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function validAffiliateApiKey()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function whitelistNetworkApiIp()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
