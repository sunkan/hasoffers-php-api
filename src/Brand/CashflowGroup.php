<?php

namespace HasOffers\Brand;

use HasOffers\Model;
use InvalidArgumentException;

class CashflowGroup extends AService
{
    public function createCashflowGroup()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findCashflowGroupById($cashflow_group_id)
    {
        if (!is_int($cashflow_group_id) || $cashflow_group_id < 1) {
            throw new InvalidArgumentException('cashflow_group_id must be an integer and be positive');
        }
        $payload = [];

        $payload['cashflow_group_id'] = $cashflow_group_id;

        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return new Model\CashflowGroup($response);
    }
    public function findCashflowGroups()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response['records'];
    }
    public function findCashflowGroupUsage()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getCashflowRuleFieldDefinitions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getGoalPayoutGroupsForGoal()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getGoalRevenueGroupsForGoal()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getOfferPayoutGroupsForOffer()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getOfferRevenueGroupsForOffer()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function replaceGoalPayoutGroupsForGoal()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function replaceGoalRevenueGroupsForGoal()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function replaceOfferPayoutGroupsForOffer()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function replaceOfferRevenueGroupsForOffer()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateCashflowGroup()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
