<?php

namespace HasOffers\Brand;

use HasOffers\Model;
use InvalidArgumentException;

class Affiliate extends AService
{
    protected $target = 'Affiliate';

    public function addAccountNote($id, $note)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }

        $response = $this->makeRequest('addAccountNote', [
            'id' => $id,
            'note' => $note,
        ]);

        return $response['data']['id'];
    }

    public function adjustAffiliateClicks($datetime, $affiliate_id, $action, $quantity, $offer_id, $goal_id = false)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!is_int($offer_id) || $offer_id < 1) {
            throw new InvalidArgumentException('offer_id must be an integer and be positive');
        }
        if (!in_array($action, ['add', 'remove'])) {
            throw new InvalidArgumentException("action must be 'add' or 'remove'");
        }
        if (!is_int($quantity)) {
            throw new InvalidArgumentException('quantity must be an integer');
        }
        $datetime = $this->_getDate($datetime);
        $payload = [
            'datetime' => $datetime->format('c'),
            'affiliate_id' => $affiliate_id,
            'action' => $action,
            'quantity' => $quantity,
            'offer_id' => $offer_id,
        ];
        if ($goal_id && is_int($goal_id) && $goal_id > 0) {
            $payload['goal_id'] = $goal_id;
        }

        $response = $this->makeRequest('adjustAffiliateClicks', $payload);

        return $response['data'];
    }

    public function block($id, $reason = '')
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }

        $response = $this->makeRequest('block', [
            'id' => $id,
            'reason' => $reason,
        ]);

        return $response['data'];
    }

    public function create($data, $return_object = true)
    {
        if (!($data instanceof Model\Affiliate)) {
            $data = new Model\Affiliate($data);
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }

        $response = $this->makeRequest('create', [
            'data' => $data,
            'return_object' => $return_object,
        ]);
        if ($return_object) {
            $data->update($response['Affiliate']);

            return $data;
        }

        return $response['data'];
    }

    public function createSignupQuestion($data)
    {
        if (!($data instanceof Model\SignupQuestion)) {
            $data = new Model\SignupQuestion($data);
        }
        $response = $this->makeRequest('createSignupQuestion', [
            'data' => $data,
        ]);

        return $response['data'];
    }

    public function createSignupQuestionAnswer($id, $data)
    {
        if (!($data instanceof Model\SignupAnswer)) {
            $data = new Model\SignupAnswer($data);
        }
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }

        $response = $this->makeRequest('createSignupQuestionAnswer', [
            'id' => $id,
            'data' => $data,
        ]);

        return $response['data'];
    }

    public function disableFraudAlert($fraud_alert_id)
    {
        if (!is_int($fraud_alert_id) || $fraud_alert_id < 1) {
            throw new InvalidArgumentException('fraud_alert_id must be an integer and be positive');
        }

        $response = $this->makeRequest('disableFraudAlert', [
            'fraud_alert_id' => $fraud_alert_id,
        ]);

        return $response['data'];
    }

    public function enableFraudAlert($fraud_alert_id)
    {
        if (!is_int($fraud_alert_id) || $fraud_alert_id < 1) {
            throw new InvalidArgumentException('fraud_alert_id must be an integer and be positive');
        }

        $response = $this->makeRequest('enableFraudAlert', [
            'fraud_alert_id' => $fraud_alert_id,
        ]);

        return $response['data'];
    }

    public function findAll(
        array $filters,
        array $sort = [],
        $limit = 25,
        $page = 1,
        array $fields = [],
        array $contain = []
    ) {
        if (!is_int($page) || $page < 1) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if (!is_int($limit) || $limit < 1) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [
            'filters' => $filters,
            'limit' => $limit,
            'page' => $page,
        ];

        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest('findAll', $payload);
        $rows = [];
        foreach ($response['data'] as $row) {
            $row2 = $row;
            unset($row2['AffiliateUser']);
            $model = new Model\Affiliate($row['Affiliate']);
            if ($row['AffiliateUser']) {
                $model->setChildModel('AffiliateUser', new Model\AffiliateUser($row['AffiliateUser']));
            }
            $rows[] = $model;
        }

        return $rows;
    }

    public function findAllByIds(array $ids, array $fields = [], array $contain = [])
    {
        $payload = [
            'ids' => $ids,
        ];

        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest('findAllIds', $payload);
        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\Affiliate($row);
        }

        return $rows;
    }

    public function findAllFraudAlerts(
        array $filters,
        array $sort = [],
        $limit = 25,
        $page = 1,
        array $fields = [],
        array $contain = []
    ) {
        if (!is_int($page) || $page < 1) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if (!is_int($limit) || $limit < 1) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [
            'filters' => $filters,
            'limit' => $limit,
            'page' => $page,
        ];

        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest('findAllFraudAlerts', $payload);
        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AffiliateFraudAlert($row);
        }

        return $rows;
    }

    public function findAllIds(array $filters = [], $limit = 25, $page = 1)
    {
        if (!is_int($page) || $page < 1) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if (!is_int($limit) || $limit < 1) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [
            'filters' => $filters,
            'limit' => $limit,
            'page' => $page,
        ];

        $response = $this->makeRequest('findAllIds', $payload);

        return $response['data'];
    }

    public function findAllIdsByAccountManagerId($employee_id = false)
    {
        if ($employee_id && (!is_int($employee_id) || $employee_id < 1)) {
            throw new InvalidArgumentException('employee_id must be an integer and be positive');
        }

        $response = $this->makeRequest('findAllIdsByAccountManagerId', !$employee_id ? [] : [
            'employee_id' => $employee_id,
        ]);

        return $response['data'];
    }

    public function findAllPendingUnassignedAffiliateIds($manager_id = false)
    {
        if ($manager_id && (!is_int($manager_id) || $manager_id < 1)) {
            throw new InvalidArgumentException('manager_id must be an integer and be positive');
        }
        $response = $this->makeRequest('findAllPendingUnassignedAffiliateIds', !$manager_id ? [] : [
            'manager_id' => $manager_id,
        ]);

        return $response['data'];
    }

    public function findAllPendingUnassignedAffiliates($employee_id = false)
    {
        if ($employee_id && (!is_int($employee_id) || $employee_id < 1)) {
            throw new InvalidArgumentException('employee_id must be an integer and be positive');
        }
        $response = $this->makeRequest('findAllPendingUnassignedAffiliates', !$employee_id ? [] : [
            'employee_id' => $employee_id,
        ]);
        $rows = [];
        foreach ($response as $row) {
            $row2 = $row;
            unset($row2['AffiliateUser']);
            $model = new Model\Affiliate($row2);
            $model->setChildModel('AffiliateUser', new Model\AffiliateUser($row['AffiliateUser']));
            $rows[] = $model;
        }

        return $rows;
    }

    public function findById($id, array $fields = [], array $contain = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [
            'id' => $id,
        ];

        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest('findById', $payload);
        $model = new Model\Affiliate($response['Affiliate']);

        if (count($contain)) {
            foreach ($contain as $key) {
                if (isset($response[$key])) {
                    $modelClass = "\HasOffers\Model\\" . $key;
                    $model->setChildModel(strtolower($key), new $modelClass($response[$key]));
                }
            }

            return $model;
        }

        return $model;
    }

    public function findList(array $filter = [])
    {
        $payload = [];
        if (count($filter)) {
            $payload['filter'] = $filter;
        }
        $response = $this->makeRequest('findList', $payload);

        return $response['data'];
    }

    public function getAccountManager($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $obj = new Model\Employee($response['data']);

        return $obj;
    }

    public function getAccountNotes($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AccountNote($row);
        }

        return $rows;
    }

    public function getAffiliateTier($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        if ($response['AffiliateTier']) {
            return new Model\AffiliateTier($response['AffiliateTier']);
        }

        return;
    }

    public function getApprovedOfferIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function getBlockedOfferIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function getBlockedReasons($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function getCreatorUser($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $obj = new Model\AffiliateUser($response['data']);

        return $obj;
    }

    public function getOfferConversionCaps($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\OfferConversionCap($row);
        }

        return $rows;
    }

    public function getOfferHostnames($id, $status = false)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if ($status && !in_array($status, ['active', 'deleted'])) {
            throw new InvalidArgumentException('status must be active or deleted');
        }
        $payload = [];
        $payload['id'] = $id;
        if ($status) {
            $payload['status'] = $status;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AffiliateOffer($row);
        }

        return $rows;
    }

    public function getOfferPayouts($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);
    }

    public function getOfferPayoutsAll()
    {
        $payload = [];
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function getOfferPixels($id, $status = false)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if ($status && !in_array($status, ['active', 'pending', 'deleted', 'rejected'])) {
            throw new InvalidArgumentException('status must be active, pending, deleted or rejected');
        }
        $payload = [];
        $payload['id'] = $id;
        if ($status) {
            $payload['status'] = $status;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\OfferPixel($row);
        }

        return $rows;
    }

    public function getOwnersAffiliateAccountId()
    {
        $response = $this->makeRequest(__FUNCTION__, []);

        return $response['data'];
    }

    public function getPaymentMethods($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function getReferralAffiliateIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function getReferralCommission($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\AffiliateReferralCommission($row);
        }

        return $rows;
    }

    public function getReferringAffiliate($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\Affiliate($response['data']);
    }

    public function getSignupAnswers($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);
        $rows = [];
        foreach ($response as $row) {
            $rows[] = new Model\SignupAnswer($row['SignupAnswer']);
        }

        return $rows;
    }

    public function getSignupQuestions($status = false)
    {
        if ($status && !in_array($status, ['active', 'paused', 'deleted'])) {
            throw new InvalidArgumentException('status must be active, paused or deleted');
        }
        $payload = [];
        if ($status) {
            $payload['status'] = $status;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response as $row) {
            $rows[] = new Model\SignupQuestion($row['SignupQuestion']);
        }

        return $rows;
    }

    public function getUnapprovedOfferIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function getUnblockedOfferIds($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function removeCustomReferralCommission($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function setCustomReferralCommission($id, $data)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!($data instanceof Model\AffiliateReferralCommission)) {
            $data = new Model\AffiliateReferralCommission($data);
        }

        $payload = [];
        $payload['id'] = $id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\AffiliateReferralCommission($response['data']);
    }

    public function signup($account, $user, $meta = false, $return_object = true)
    {
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
        if (!($account instanceof Model\Affiliate)) {
            $account = new Model\Affiliate($account);
        }
        if (!($user instanceof Model\AffiliateUser)) {
            $user = new Model\AffiliateUser($user);
        }
        if ($meta && !($meta instanceof Model\AffiliateMeta)) {
            $meta = new Model\AffiliateMeta($meta);
        }

        $payload = [];
        $payload['account'] = $account;
        $payload['user'] = $user;
        if ($meta) {
            $payload['meta'] = $meta;
        }
        $payload['return_object'] = $return_object;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        if ($return_object) {
            return new Model\Affiliate($response['data']);
        }

        return $response['data'];
    }

    public function simpleSearch(
        array $filters = [],
        array $fields = [],
        $limit = false,
        $page = false,
        array $sort = []
    ) {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limt && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = $page;
        }
        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function update($id, $data, $return_object = true)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!($data instanceof Model\Affiliate)) {
            $data = new Model\Affiliate($data);
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }

        $payload = [];
        $payload['id'] = $id;
        $payload['data'] = $data;
        $payload['return_object'] = $return_object;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $return_object ? new Model\Affiliate($response['data']) : $response['data'];
    }

    public function updateAccountNote($account_note_id, $note)
    {
        if (!is_int($account_note_id) || $account_note_id < 1) {
            throw new InvalidArgumentException('account_note_id must be an integer and be positive');
        }
        $payload = [];
        $payload['account_note_id'] = $account_note_id;
        $payload['note'] = $note;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updateByRefId($ref_id, $data, $return_object = true)
    {
        if (!is_int($ref_id) || $ref_id < 1) {
            throw new InvalidArgumentException('ref_id must be an integer and be positive');
        }
        if (!($data instanceof Model\Affiliate)) {
            $data = new Model\Affiliate($data);
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }

        $payload = [];
        $payload['ref_id'] = $ref_id;
        $payload['data'] = $data;
        $payload['return_object'] = $return_object;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $return_object ? new Model\Affiliate($response['data']) : $response['data'];
    }

    public function updateField($id, $field, $value, $return_object = false)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['field'] = $field;
        $payload['value'] = $value;
        $payload['return_object'] = $return_object;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $return_object ? new Model\Affiliate($response['data']) : $response['data'];
    }

    public function updatePaymentMethodCheck($affiliate_id, $data)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!($data instanceof Model\PaymentMethodCheck)) {
            $data = new Model\PaymentMethodCheck($data);
        }
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updatePaymentMethodDirectDeposit($affiliate_id, $data)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!($data instanceof Model\PaymentMethodDirectDeposit)) {
            $data = new Model\PaymentMethodDirectDeposit($data);
        }
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response;
    }

    public function updatePaymentMethodOther($affiliate_id, $data)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!($data instanceof Model\PaymentMethodOther)) {
            $data = new Model\PaymentMethodOther($data);
        }
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updatePaymentMethodPayoneer($affiliate_id, $data)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!($data instanceof Model\PaymentMethodPayoneer)) {
            $data = new Model\PaymentMethodPayoneer($data);
        }
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updatePaymentMethodPaypal($affiliate_id, $data)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!($data instanceof Model\PaymentMethodPaypal)) {
            $data = new Model\PaymentMethodPaypal($data);
        }
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updatePaymentMethodPayQuicker($affiliate_id, $data)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!($data instanceof Model\PaymentMethodPayQuicker)) {
            $data = new Model\PaymentMethodPayQuicker($data);
        }
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updatePaymentMethodWire($affiliate_id, $data)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }
        if (!($data instanceof Model\PaymentMethodWire)) {
            $data = new Model\PaymentMethodWire($data);
        }
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;
        $payload['data'] = $data;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updateSignupQuestion($question_id, $data)
    {
        if (!is_int($question_id) || $question_id < 1) {
            throw new InvalidArgumentException('question_id must be an integer and be positive');
        }
        if (!($data instanceof Model\SignupQuestion)) {
            $data = new Model\SignupQuestion($data);
        }
        $payload = [];
        $payload['data'] = $data;
        $payload['question_id'] = $question_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }

    public function updateSignupQuestionAnswer($answer_id, $data)
    {
        if (!is_int($answer_id) || $answer_id < 1) {
            throw new InvalidArgumentException('answer_id must be an integer and be positive');
        }
        if (!($data instanceof Model\SignupAnswer)) {
            $data = new Model\SignupAnswer($data);
        }
        $payload = [];
        $payload['data'] = $data;
        $payload['answer_id'] = $answer_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
