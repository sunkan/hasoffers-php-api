<?php

namespace HasOffers\Brand;

use HasOffers\Model;
use InvalidArgumentException;

class AffiliateUser extends AService
{
    public function checkPassword($id, $password)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['password'] = $password;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function create($data, $return_object = true)
    {
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
        if (!($data instanceof Model\AffiliateUser)) {
            $data = new Model\AffiliateUser($data);
        }
        $payload = [];
        $payload['data'] = $data;
        $payload['return_object'] = $return_object;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $return_object ? new Model\AffiliateUser($response['AffiliateUser']) : $response;
    }
    public function findAll(array $filters = [], array $sort = [], $limit = false, $page = false, array $fields = [], array $contain = [])
    {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limt && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = $page;
        }
        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            if ($row['Affiliate']) {
                $affiliate = new Model\Affiliate($row['Affiliate']);
                unset($row['Affiliate']);
            }
            $user = new Model\AffiliateUser($row['AffiliateUser']);
            if ($affiliate) {
                $user->setChildModel('affiliate', $affiliate);
            }
            $rows[] = $user;
        }

        return $rows;
    }
    public function findAllByIds(array $ids, array $fields = [], array $contain = [])
    {
        $payload = [];
        $payload['ids'] = $ids;
        if (count($fields)) {
            $payload['fields'] = $fields;
        }
        if (count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);
        $rows = [];
        foreach ($response as $row) {
            $rows[] = new Model\AffiliateUser($row['AffiliateUser']);
        }

        return $rows;
    }
    public function findAllIds()
    {
        $response = $this->makeRequest(__FUNCTION__, []);

        return $response['data'];
    }
    public function findAllIdsByAffiliateId($affiliate_id)
    {
        if (!is_int($affiliate_id) || $affiliate_id < 1) {
            throw new InvalidArgumentException('affiliate_id must be an integer and be positive');
        }

        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response;
    }
    public function findById($id, array $fields = [], array $contain = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        if (count($fields)) {
            $payload['fields'] = $fields;
        }
        if (count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);
        $model = false;
        if ($response['AffiliateUser']) {
            $model = new Model\AffiliateUser($response['AffiliateUser']);
        }
        if (count($contain)) {
            foreach ($contain as $key) {
                if (isset($response[$key])) {
                    $modelClass = "\HasOffers\Model\\".$key;
                    $model->setChildModel(strtolower($key), new $modelClass($response[$key]));
                }
            }

            return $model;
        }

        return;
    }
    public function grantAccess($id, $permission)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!in_array($permission, ['Affiliate.account_management', 'Affiliate.API', 'Affiliate.offer_management', 'Affiliate.stats', 'Affiliate.user_management'])) {
            throw new InvalidArgumentException('invalid permission');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['permission'] = $permission;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function removeAccess($id, $permission)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!in_array($permission, ['Affiliate.account_management', 'Affiliate.API', 'Affiliate.offer_management', 'Affiliate.stats', 'Affiliate.user_management'])) {
            throw new InvalidArgumentException('invalid permission');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['permission'] = $permission;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function resetPassword($id, $length = 8)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['length'] = $length;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function uniqueEmail($email)
    {
        $payload = [];
        $payload['email'] = $email;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response;
    }
    public function update($id, $data, $return_object = true)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!($data instanceof Model\AffiliateUser)) {
            $data = new Model\AffiliateUser($data);
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['data'] = $data;
        $payload['return_object'] = $return_object;
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $return_object ? new Model\AffiliateUser($response['data']) : $response['data'];
    }
    public function updateField($id, $field, $value, $return_object = false)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_bool($return_object)) {
            throw new InvalidArgumentException('return_object must be a boolean');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['field'] = $field;
        $payload['value'] = $value;
        $payload['return_object'] = $return_object;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $return_object ? new Model\AffiliateUser($response['data']) : $response['data'];
    }
}
