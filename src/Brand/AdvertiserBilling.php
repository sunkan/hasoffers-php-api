<?php

namespace HasOffers\Brand;

use HasOffers\Model;

class AdvertiserBilling extends AService
{
    public function addInvoiceItem()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createInvoice()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createReceipt()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllInvoices()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllInvoicesByIds()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllReceipts()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllReceiptsByIds()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findInvoiceById()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findInvoiceStats()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findLastInvoice($affiliate_id)
    {
        $payload = [];
        $payload['affiliate_id'] = $affiliate_id;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return new Model\AffiliateInvoice($response['AffiliateInvoice']);
    }
    public function findLastReceipt()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findReceiptById()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function generateInvoices()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getAccountBalance()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getAccountHistory()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getNextStartDate()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getOutstandingInvoices()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getPayoutTotals()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function removeInvoiceItem()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateInvoice()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateInvoiceField()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateReceipt()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateReceiptField()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateTaxInfo()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
