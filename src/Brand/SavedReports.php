<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class SavedReports extends AService
{
    public function create()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__);

        return $response['data'];
    }
    public function delete()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__);

        return $response['data'];
    }
    public function findByHash()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__);

        return $response['data'];
    }
    public function findById()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__);

        return $response['data'];
    }
    public function update()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__);

        return $response['data'];
    }
}
