<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class CustomPage extends AService
{
    public function find()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findById()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
