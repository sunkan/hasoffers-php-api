<?php

namespace HasOffers\Brand;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class TinyUrl extends AService
{
    public function findAll(array $filters = [], array $sort = [], $limit = false, $page = false, array $fields = [], array $contain = [])
    {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limit && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [
            'limit' => $limit,
            'page' => $page,
        ];
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateRedirect($id, $redirect)
    {
        $payload = [];
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!($redirect instanceof Model\TinyUrl)) {
            $redirect = new Model\TinyUrl($redirect);
        }
        $payload['id'] = $id;
        $payload['redirect'] = $redirect;

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
