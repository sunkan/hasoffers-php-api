<?php

namespace HasOffers\Brand;

use DateTime;

class Report extends AService
{
    public function getActiveCurrencies()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getAffiliateCommissions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getConversions(array $fields, array $groups = [],
                                array $filters = [], array $sort = [], $limit = false,
                                $page = false, $totals = false, $data_start = false,
                                $data_end = false, $count = false, $hour_offset = false, $status_update_fields = false)
    {
        $payload = [
            'fields' => $fields,
            'totals' => $totals,
            'count' => $count,
        ];
        if (count($groups)) {
            $payload['groups'] = $groups;
        }
        if (count($filters)) {
            $payload['filters'] = $filters;
        }
        if (count($sort)) {
            $payload['sort'] = $sort;
        }
        if (is_int($limit) && $limit > 0) {
            $payload['limit'] = $limit;
        }
        if (is_int($page) && $page > 0) {
            $payload['page'] = $page;
        }
        if ($status_update_fields) {
            $payload['status_update_fields'] = $status_update_fields;
        }

        if ($data_start) {
            if (!($data_start instanceof DateTime)) {
                $data_start = new DateTime($data_start);
            }
            $payload['data_start'] = $data_start->format('Y-m-d');
        }
        if ($data_end) {
            if (!($data_end instanceof DateTime)) {
                $data_end = new DateTime($data_end);
            }
            $payload['data_end'] = $data_end->format('Y-m-d');
        }
        if (is_int($hour_offset)) {
            $payload['hour_offset'] = $hour_offset;
        }

        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response;
    }
    public function getManagerCommissions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getModSummaryLogs()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getReferrals()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getStats(array $fields, array $groups = [], array $filters = [],
                             array $sort = [], $limit = false, $page = false,
                             $totals = true, $currency = false, $data_start = false,
                             $data_end = false, $hour_offset = false)
    {
        $payload = [
            'fields' => $fields,
            'totals' => $totals,
        ];
        if (count($groups)) {
            $payload['groups'] = $groups;
        }
        if (count($filters)) {
            $payload['filters'] = $filters;
        }
        if (count($sort)) {
            $payload['sort'] = $sort;
        }
        if (is_int($limit) && $limit > 0) {
            $payload['limit'] = $limit;
        }
        if (is_int($page) && $page > 0) {
            $payload['page'] = $page;
        }
        if ($currency && strlen($currency) === 3) {
            $payload['currency'] = $currency;
        }

        if ($data_start) {
            if (!($data_start instanceof DateTime)) {
                $data_start = new DateTime($data_start);
            }
            $payload['data_start'] = $data_start->format('Y-m-d');
        }
        if ($data_end) {
            if (!($data_end instanceof DateTime)) {
                $data_end = new DateTime($data_end);
            }
            $payload['data_end'] = $data_end->format('Y-m-d');
        }
        if (is_int($hour_offset)) {
            $payload['hour_offset'] = $hour_offset;
        }
        $response = $this->makeRequest(__FUNCTION__, $payload, 'get');

        return $response;
    }
    public function getSubscriptions()
    {
        $payload = [];

        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
