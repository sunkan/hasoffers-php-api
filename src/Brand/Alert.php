<?php

namespace HasOffers\Brand;

use HasOffers\AService;

class Alert extends AService
{
    public function create()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createAffiliateUserAlert()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function createEmployeeAlert()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function dismissAffiliateUserAlert()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function dismissAllAffiliateUserAlerts()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function dismissAllEmployeeAlerts()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function dismissEmployeeAlert()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAll()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findAllByIds()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function findById()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getAffiliateUserAlerts()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function getEmployeeAlerts()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function sendToAffiliateUsers()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function sendToEmployees()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function update()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
    public function updateField()
    {
        $response = $this->makeRequest(__FUNCTION__, $payload);

        return $response['data'];
    }
}
