<?php

namespace HasOffers\Model;

class AffiliateUserAlert extends AModel
{
    protected $fields = [
        'affiliate_user_id' => 'integer',
        'alert_id' => 'integer',
        'id' => 'integer',
        'is_dismissed' => 'boolean',
    ];
}
