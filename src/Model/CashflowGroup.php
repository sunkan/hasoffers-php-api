<?php

namespace HasOffers\Model;

class CashflowGroup extends AModel
{
    protected $fields = [
        'affiliates' => 'array',
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'rules' => 'array',
    ];
}
