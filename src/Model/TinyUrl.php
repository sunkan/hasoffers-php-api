<?php

namespace HasOffers\Model;

class TinyUrl extends AModel
{
    protected $fields = [
        'action' => [
            'Goal Impression',
            'Goal Click',
            'Goal Postback',
            'Goal Mobile Postback',
            'Goal Pixel',
            'Click',
            'Offer Pixel',
            'Offer Postback',
            'Offer Mobile Postback',
        ],
        'affiliate_id' => 'integer',
        'hash' => 'string',
        'id' => 'integer',
        'offer_id' => 'integer',
        'redirect' => 'string',
        'short_url' => 'string',
    ];
}
