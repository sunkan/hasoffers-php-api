<?php

namespace HasOffers\Model;

class Country extends AModel
{
    protected $fields = [
        'code' => 'string',
        'id' => 'integer',
        'name' => 'string',
        'regions' => 'array',
    ];
}
