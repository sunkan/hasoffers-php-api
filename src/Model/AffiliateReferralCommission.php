<?php

namespace HasOffers\Model;

class AffiliateReferralCommission extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'field' => ['payout', 'revenue', 'profit'],
        'rate' => 'decimal',
        'rate_type' => ['flat', 'percentage'],
    ];
}
