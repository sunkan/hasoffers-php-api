<?php

namespace HasOffers\Model;

class TargetRule extends AModel
{
    protected $fields = [
        'category' => 'string',
        'description' => 'string',
        'id' => 'integer',
        'name' => 'string',
        'req_affiliate_info1' => 'string',
        'req_affiliate_info2' => 'string',
        'req_affiliate_info3' => 'string',
        'req_affiliate_info4' => 'string',
        'req_affiliate_info5' => 'string',
        'req_browser_name' => 'string',
        'req_browser_version' => 'string',
        'req_connection_speed' => 'string',
        'req_device_brand' => 'string',
        'req_device_marketing_name' => 'string',
        'req_device_model' => 'string',
        'req_device_os' => 'string',
        'req_device_os_version' => 'string',
        'req_language' => 'string',
        'req_mobile_carrier' => 'string',
        'req_user_agent' => 'string',
        'source' => 'string',
    ];
}
