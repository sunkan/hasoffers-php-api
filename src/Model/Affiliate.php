<?php

namespace HasOffers\Model;

class Affiliate extends AModel
{
    protected $fields = [
        'account_manager_id' => 'integer',
        'affiliate_tier_id' => 'integer',
        'address1' => 'string',
        'address2' => 'string',
        'city' => 'string',
        'company' => 'string',
        'country' => 'string',
        'date_added' => 'DateTime',
        'fax' => 'string',
        'fraud_activity_alert_threshold' => 'integer',
        'fraud_activity_block_threshold' => 'integer',
        'fraud_activity_score' => 'integer',
        'fraud_profile_alert_threshold' => 'integer',
        'fraud_profile_block_threshold' => 'integer',
        'fraud_profile_score' => 'integer',
        'id' => 'integer',
        'modified' => 'DateTime',
        'payment_method' => ['wire', 'check', 'paypal', 'other', 'direct_deposit', 'payoneer', 'payquicker'],
        'payment_terms' => ['net 7', 'net 15', 'net 30', 'net 60', 'net 90', 'invoice', 'other'],
        'phone' => 'string',
        'ref_id' => 'string',
        'referral_id' => 'integer',
        'region' => 'string',
        'signup_ip' => 'string',
        'status' => ['pending', 'active', 'blocked', 'deleted', 'rejected'],
        'w9_filed' => 'boolean',
        'zipcode' => 'string',
    ];
}
