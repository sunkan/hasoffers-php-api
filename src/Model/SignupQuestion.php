<?php

namespace HasOffers\Model;

class SignupQuestion extends AModel
{
    protected $fields = [
        'class' => ['bool', 'select', 'textarea'],
        'class_data' => 'string',
        'id' => 'integer',
        'question' => 'integer',
        'required' => 'boolean',
        'status' => ['active', 'paused', 'deleted'],
        'type' => ['affiliate', 'offer'],
        'type_id' => 'integer',
    ];
}
