<?php

namespace HasOffers\Model;

class AdCampaignCreative extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'modified' => 'DateTime',
        'custom_weight' => 'decimal',
        'ad_campaign_id' => 'integer',
        'offer_file_id' => 'integer',
        'offer_url_id' => 'integer',
        'offer_id' => 'integer',
        'status' => ['active', 'paused', 'deleted'],
    ];
}
