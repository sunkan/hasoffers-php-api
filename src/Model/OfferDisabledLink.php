<?php

namespace HasOffers\Model;

class OfferDisabledLink extends AModel
{
    protected $fields = [
        'aff_info1' => 'string',
        'aff_info2' => 'string',
        'aff_info3' => 'string',
        'aff_info4' => 'string',
        'aff_info5' => 'string',
        'affiliate_id' => 'integer',
        'datetime' => 'DateTime',
        'id' => 'integer',
        'offer_id' => 'integer',
        'source' => 'string',
        'strict' => 'boolean',
    ];
}
