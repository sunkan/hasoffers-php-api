<?php

namespace HasOffers\Model;

class AdvertiserApiKey extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'api_key' => 'string',
        'id' => 'integer',
        'status' => ['active', 'paused', 'pending', 'deleted', 'rejected'],
    ];
}
