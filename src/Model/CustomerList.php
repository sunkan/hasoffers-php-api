<?php

namespace HasOffers\Model;

class CustomerList extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'modified' => 'DateTime',
        'name' => 'string',
        'status' => ['active', 'deleted'],
    ];
}
