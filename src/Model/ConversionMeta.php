<?php

namespace HasOffers\Model;

class ConversionMeta extends AModel
{
    protected $fields = [
        'conversion_id' => 'integer',
        'note' => 'string',
    ];
}
