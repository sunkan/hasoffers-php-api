<?php

namespace HasOffers\Model;

class AccountNote extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'type' => ['affiliate', 'advertiser'],
        'account_id' => 'integer',
        'note' => 'string',
        'created' => 'DateTime',
    ];
}
