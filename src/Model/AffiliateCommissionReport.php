<?php

namespace HasOffers\Model;

class AffiliateCommissionReport extends AModel
{
    protected $fields = [
        'Affiliate.company' => 'string',
        'AffiliateManager.full_name' => 'string',
        'ReferredAffiliate.company' => 'string',
        'Stat.affiliate_id' => 'integer',
        'Stat.affiliate_manager_id' => 'integer',
        'Stat.amount' => 'decimal',
        'Stat.date' => 'Date',
        'Stat.rate' => 'decimal',
        'Stat.rate_type' => ['flat', 'percentage'],
        'Stat.referral_id' => 'integer',
        'Stat.var_total' => 'decimal',
    ];
}
