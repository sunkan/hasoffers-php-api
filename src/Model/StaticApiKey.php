<?php

namespace HasOffers\Model;

class StaticApiKey extends AModel
{
    protected $fields = [
        'api_key' => 'string',
        'created' => 'DateTime',
        'id' => 'integer',
        'status' => ['active', 'pending', 'rejected'],
        'updated' => 'DateTime',
        'user_id' => 'integer',
        'user_type' => ['affiliate'],
    ];
}
