<?php

namespace HasOffers\Model;

class Employee extends AModel
{
    protected $fields = [
        'access' => 'array',
        'aim' => 'string',
        'billing_user' => 'boolean',
        'cell_phone' => 'string',
        'email' => 'string',
        'first_name' => 'string',
        'id' => 'integer',
        'join_date' => 'DateTime',
        'last_login' => 'DateTime',
        'last_name' => 'string',
        'modified' => 'DateTime',
        'password' => 'string',
        'password_confirmation' => 'string',
        'permissions' => 'integer',
        'phone' => 'string',
        'photo' => 'string',
        'status' => ['active', 'deleted'],
        'title' => 'string',
    ];
}
