<?php

namespace HasOffers\Model;

class PaymentMethodPaypal extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'email' => 'string',
        'modified' => 'DateTime',
    ];
}
