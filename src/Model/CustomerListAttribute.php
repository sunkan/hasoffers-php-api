<?php

namespace HasOffers\Model;

class CustomerListAttribute extends AModel
{
    protected $fields = [
        'attribute' => 'string',
        'customer_list_id' => 'integer',
        'id' => 'integer',
        'modified' => 'DateTime',
    ];
}
