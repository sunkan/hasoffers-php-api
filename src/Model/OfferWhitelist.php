<?php

namespace HasOffers\Model;

class OfferWhitelist extends AModel
{
    protected $fields = [
        'content' => 'string',
        'content_type' => ['domain', 'ip_address', 'ip_range', 'ip_subnet'],
        'datetime' => 'DateTime',
        'id' => 'integer',
        'offer_id' => 'integer',
        'type' => ['postback', 'refer'],
    ];
}
