<?php

namespace HasOffers\Model;

class CustomMenuLink extends AModel
{
    protected $fields = [
        'custom_page_id' => 'integer',
        'external_url' => 'string',
        'id' => 'integer',
        'interface' => ['affiliate', 'advertiser', 'employee'],
        'location' => ['internal', 'external'],
        'placement' => ['top','bottom'],
        'position' => 'integer',
        'tab' => ['account', 'ad_manager', 'offers', 'snapshot', 'stats', 'support', 'reports', 'affiliates', 'advertisers', 'company'],
        'title' => 'string',
        'type' => ['tab', 'link'],
    ];
}
