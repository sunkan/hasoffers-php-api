<?php

namespace HasOffers\Model;

class EmployeeCommission extends AModel
{
    protected $fields = [
        'employee_id' => 'integer',
        'field' => ['leads', 'payout', 'revenue', 'profit'],
        'min_commission' => 'decimal',
        'rate' => 'decimal',
        'rate_type' => ['flat', 'percentage'],
    ];
}
