<?php

namespace HasOffers\Model;

class ServerLogsReport extends AModel
{
    protected $fields = [
        'Affiliate.company' => 'string',
        'ModSummaryLog.action' => 'string',
        'ModSummaryLog.affiliate_id' => 'integer',
        'ModSummaryLog.count' => 'string',
        'ModSummaryLog.country_code' => 'string',
        'ModSummaryLog.date' => 'Date',
        'ModSummaryLog.message' => 'string',
        'ModSummaryLog.offer_id' => 'integer',
        'ModSummaryLog.region' => 'string',
        'ModSummaryLog.type' => 'string',
        'Offer.name' => 'string',
    ];
}
