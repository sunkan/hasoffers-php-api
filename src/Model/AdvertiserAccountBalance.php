<?php

namespace HasOffers\Model;

class AdvertiserAccountBalance extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'balance' => 'decimal',
    ];
}
