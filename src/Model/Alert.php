<?php

namespace HasOffers\Model;

class Alert extends AModel
{
    protected $fields = [
        'content' => 'string',
        'data' => 'string',
        'date' => 'Date',
        'datetime' => 'DateTime',
        'description' => 'string',
        'employee_id' => 'integer',
        'id' => 'integer',
        'ref_affiliate_id' => 'integer',
        'status' => ['active', 'deleted'],
        'title' => 'string',
        'type_code' => [100],
    ];
}
/*
"100" is a custom Alert that could contain any message.
"201": New Affiliate.
"202": New pending Affiliate.
"203": Affiliate Manager assigned.
"301": New Advertiser.
"302": New pending Advertiser.
"303": Advertiser Manager assigned.
"401": Affiliate activity fraud block.
"402": Affiliate activity fraud alert.
"501": Affiliate profile fraud block.
"502": Affiliate profile fraud alert.
"601": Offer Application has been approved.
"602": Offer Application has been denied.
"603": Offer Application is pending.
*/
