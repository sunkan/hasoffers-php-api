<?php

namespace HasOffers\Model;

class AffiliateOffer extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'agreed_terms_and_conditions' => 'Date',
        'approval_notes' => 'string',
        'approval_status' => 'string',
        'hostname_id' => 'integer',
        'id' => 'integer',
        'offer_id' => 'integer',
    ];
}
