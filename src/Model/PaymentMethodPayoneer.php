<?php

namespace HasOffers\Model;

class PaymentMethodPayoneer extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'status' => ['pending', 'registered', 'approved', 'declined'],
    ];
}
