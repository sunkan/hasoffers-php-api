<?php

namespace HasOffers\Model;

class OfferUrl extends AModel
{
    protected $fields = [
        'created' => 'DateTime',
        'id' => 'integer',
        'modified' => 'DateTime',
        'name' => 'string',
        'offer_id' => 'integer',
        'offer_url' => 'string',
        'preview_url' => 'string',
        'status' => ['active', 'deleted'],
    ];
}
