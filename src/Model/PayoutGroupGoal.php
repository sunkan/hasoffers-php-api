<?php

namespace HasOffers\Model;

class PayoutGroupGoal extends AModel
{
    protected $fields = [
        'cashflow_group_id' => 'array',
        'goal_id' => 'integer',
        'id' => 'integer',
        'percent' => 'decimal',
        'rate' => 'decimal',
    ];
}
