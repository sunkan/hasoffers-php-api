<?php

namespace HasOffers\Model;

class EmployeeAlert extends AModel
{
    protected $fields = [
        'alert_id' => 'integer',
        'employee_id' => 'integer',
        'id' => 'integer',
        'is_dismissed' => 'boolean',
    ];
}
