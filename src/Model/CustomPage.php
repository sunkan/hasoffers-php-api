<?php

namespace HasOffers\Model;

class CustomPage extends AModel
{
    protected $fields = [
        'html' => 'string',
        'id' => 'integer',
        'status' => ['active', 'inactive', 'deleted'],
        'title' => 'string',
    ];
}
