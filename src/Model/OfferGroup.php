<?php

namespace HasOffers\Model;

class OfferGroup extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'date_created' => 'DateTime',
        'date_updated' => 'DateTime',
        'name' => 'string',
        'status' => 'string',
    ];
}
