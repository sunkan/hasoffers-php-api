<?php

namespace HasOffers\Model;

class AffiliateTierRevenue extends AModel
{
    protected $fields = [
        'affiliate_tier_id' => 'integer',
        'goal_id' => 'integer',
        'id' => 'integer',
        'modified' => 'DateTime',
        'offer_id' => 'integer',
        'revenue' => 'decimal',
        'percent_revenue' => 'decimal',
    ];
}
