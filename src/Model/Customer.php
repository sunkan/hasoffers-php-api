<?php

namespace HasOffers\Model;

class Customer extends AModel
{
    protected $fields = [
        'customer_list_id' => 'integer',
        'id' => 'integer',
        'provided_id' => 'string',
    ];
}
