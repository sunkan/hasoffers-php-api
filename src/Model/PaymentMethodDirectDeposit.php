<?php

namespace HasOffers\Model;

class PaymentMethodDirectDeposit extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'account_holder' => 'string',
        'account_number' => 'string',
        'bank_name' => 'string',
        'other_details' => 'string',
        'routing_number' => 'string',
    ];
}
