<?php

namespace HasOffers\Model;

class Goal extends AModel
{
    protected $fields = [
        'advertiser_id' => 'integer',
        'allow_multiple_conversions' => 'boolean',
        'approve_conversions' => 'boolean',
        'default_payout' => 'decimal',
        'description' => 'string',
        'display_advertiser' => 'boolean',
        'enforce_encrypt_tracking_pixels' => 'boolean',
        'id' => 'integer',
        'is_end_point' => 'boolean',
        'is_private' => 'boolean',
        'max_payout' => 'decimal',
        'max_percent_payout' => 'decimal',
        'modified' => 'DateTime',
        'name' => 'string',
        'offer_id' => 'integer',
        'payout_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both'],
        'percent_payout' => 'decimal',
        'protocol' => ['http', 'https', 'http_img', 'https_img', 'server', 'server_affiliate', 'mobile_app', 'mobile_device'],
        'ref_id' => 'string',
        'revenue_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both'],
        'status' => ['active', 'deleted'],
        'tiered_payout' => 'boolean',
        'tiered_revenue' => 'boolean',
        'use_payout_groups' => 'boolean',
        'use_revenue_groups' => 'boolean',
    ];
}
