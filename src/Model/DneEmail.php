<?php

namespace HasOffers\Model;

class DneEmail extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'email' => 'string',
        'ip' => 'string',
        'datetime' => 'DateTime',
    ];
}
