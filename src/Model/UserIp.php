<?php

namespace HasOffers\Model;

class UserIp extends AModel
{
    protected $fields = [
        'brand_id' => 'integer',
        'failed_logins' => 'integer',
        'first_login' => 'DateTime',
        'id' => 'integer',
        'ip_address' => 'string',
        'last_login' => 'DateTime',
        'login_type' => ['browser', 'api'],
        'user_type' => ['affiliate_user', 'advertiser_user', 'employee'],
        'user_type_id' => 'integer',
        'valid_logins' => 'integer',
    ];
}
