<?php

namespace HasOffers\Model;

class Subscription extends AModel
{
    protected $fields = [
        'advertiser_info' => 'string',
        'affiliate_id' => 'integer',
        'affiliate_info1' => 'string',
        'affiliate_info2' => 'string',
        'affiliate_info3' => 'string',
        'affiliate_info4' => 'string',
        'affiliate_info5' => 'string',
        'affiliate_manager_id' => 'integer',
        'created' => 'DateTime',
        'currency' => 'string',
        'customer_id' => 'integer',
        'customer_list_id' => 'integer',
        'end_date' => 'Date',
        'id' => 'integer',
        'offer_id' => 'integer',
        'payout' => 'decimal',
        'payout_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both', 'cpc', 'cpm'],
        'pixel_refer' => 'string',
        'provided_id' => 'string',
        'refer' => 'string',
        'revenue' => 'decimal',
        'revenue_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both', 'cpc', 'cpm'],
        'sale_amount' => 'decimal',
        'source' => 'string',
        'start_date' => 'Date',
        'status' => ['active', 'deleted'],
    ];
}
