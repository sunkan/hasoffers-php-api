<?php

namespace HasOffers\Model;

class ReferralReport extends AModel
{
    protected $fields = [
        'Advertiser.company' => 'string',
        'AdvertiserManager.full_name' => 'string',
        'Affiliate.company' => 'string',
        'AffiliateManager.full_name' => 'string',
        'Offer.name' => 'string',
        'Stat.advertiser_id' => 'integer',
        'Stat.advertiser_manager_id' => 'integer',
        'Stat.affiliate_id' => 'integer',
        'Stat.affiliate_info1' => 'string',
        'Stat.affiliate_info2' => 'string',
        'Stat.affiliate_info3' => 'string',
        'Stat.affiliate_info4' => 'string',
        'Stat.affiliate_info5' => 'string',
        'Stat.affiliate_manager_id' => 'integer',
        'Stat.clicks' => 'integer',
        'Stat.conversions' => 'integer',
        'Stat.count' => 'integer',
        'Stat.date' => 'Date',
        'Stat.offer_id' => 'integer',
        'Stat.source' => 'string',
        'Stat.url' => 'string',
    ];
}
