<?php

namespace HasOffers\Model;

class PaymentMethodWire extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'account_number' => 'string',
        'bank_name' => 'string',
        'beneficiary_name' => 'string',
        'modified' => 'DateTime',
        'other_details' => 'string',
        'routing_number' => 'string',
    ];
}
