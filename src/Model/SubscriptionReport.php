<?php

namespace HasOffers\Model;

class SubscriptionReport extends AModel
{
    protected $fields = [
        'Affiliate.company' => 'string',
        'AffiliateManager.full_name' => 'string',
        'CustomerList.name' => 'string',
        'Offer.name' => 'string',
        'Stat.advertiser_info' => 'string',
        'Stat.affiliate_id' => 'integer',
        'Stat.affiliate_info1' => 'string',
        'Stat.affiliate_info2' => 'string',
        'Stat.affiliate_info3' => 'string',
        'Stat.affiliate_info4' => 'string',
        'Stat.affiliate_info5' => 'string',
        'Stat.affiliate_manager_id' => 'integer',
        'Stat.created' => 'DateTime',
        'Stat.customer_list_id' => 'integer',
        'Stat.end_date' => 'Date',
        'Stat.id' => 'integer',
        'Stat.offer_id' => 'integer',
        'Stat.payout_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both', 'cpc', 'cpm'],
        'Stat.pixel_refer' => 'string',
        'Stat.provided_id' => 'string',
        'Stat.refer' => 'string',
        'Stat.revenue_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both', 'cpc', 'cpm'],
        'Stat.source' => 'string',
        'Stat.start_date' => 'Date',
        'Stat.status' => ['active', 'deleted'],
    ];
}
