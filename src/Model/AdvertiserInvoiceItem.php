<?php

namespace HasOffers\Model;

class AdvertiserInvoiceItem extends AModel
{
    protected $fields = [
        'actions' => 'integer',
        'amount' => 'decimal',
        'datetime' => 'DateTime',
        'goal_id' => 'integer',
        'id' => 'integer',
        'invoice_id' => 'integer',
        'memo' => 'string',
        'offer_id' => 'integer',
        'revenue_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both', 'cpc', 'cpm', 'amount'],
        'type' => ['stats', 'adjustment', 'vat'],
        'vat_code' => 'string',
        'vat_id' => 'integer',
        'vat_name' => 'string',
        'vat_rate' => 'decimal',
    ];
}
