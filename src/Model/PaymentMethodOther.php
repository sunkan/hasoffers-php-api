<?php

namespace HasOffers\Model;

class PaymentMethodOther extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'details' => 'string',
    ];
}
