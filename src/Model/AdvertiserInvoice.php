<?php

namespace HasOffers\Model;

class AdvertiserInvoice extends AModel
{
    protected $fields = [
        'advertiser_id' => 'integer',
        'affiliate_id' => 'integer',
        'actions' => 'integer',
        'amount' => 'deciaml',
        'currency' => 'string',
        'datetime' => 'DateTime',
        'end_date' => 'Date',
        'id' => 'integer',
        'is_paid' => 'boolean',
        'memo' => 'string',
        'notes' => 'string',
        'receipt_id' => 'integer',
        'start_date' => 'Date',
        'status' => ['active', 'deleted'],
    ];
}
