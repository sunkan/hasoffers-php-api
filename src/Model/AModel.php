<?php

namespace HasOffers\Model;

use DateTime;
use DateTimeZone;

abstract class AModel
{
    protected $data = [];
    protected $fields = [];
    protected $childs = [];

    public function __construct($data = [])
    {
        $this->data = $data;
    }
    public function update(array $data)
    {
        $this->data = $data;
    }
    public function getData()
    {
        $data = [];
        foreach ($this->data as $key => $value) {
            $data[$key] = $this->castToString($key, $value);
        }

        return $data;
    }
    public function setChildModel($key, AModel $model)
    {
        $this->childs[$key] = $model;
    }
    protected function castToString($key, $value)
    {
        $field = $this->fields[$key];

        if (is_array($field)) {
            return in_array($value, $field) ? $value : false;
        } elseif ($field == 'integer') {
            return (int) $value;
        } elseif ($field == 'Date' || $field == 'DateTime') {
            if ($value instanceof DateTime) {
                return $value->format($field == 'Date' ? 'Y-m-d' : 'c');
            } else {
                return $value;
            }
        } elseif ($field == 'boolean') {
            return $value ? 1 : 0;
        } elseif ($field == 'decimal') {
            return (float) $value;
        }

        return $value;
    }
    protected function cast($key, $value)
    {
        $field = $this->fields[$key];

        if (is_array($field)) {
            return in_array($value, $field) ? $value : false;
        } elseif ($field == 'integer') {
            return (int) $value;
        } elseif ($field == 'Date' || $field == 'DateTime') {
            return new DateTime($value, new DateTimeZone('utc'));
        } elseif ($field == 'boolean') {
            return (bool) $value;
        } elseif ($field == 'decimal') {
            return (float) $value;
        }

        return $value;
    }
    protected $extras = [];
    public function __get($key)
    {
        if (isset($this->data[$key])) {
            $value = $this->data[$key];

            return $this->cast($key, $value);
        }
        if (isset($this->childs[$key])) {
            return $this->childs[$key];
        }
        if (isset($this->extras[$key])) {
            return $this->extras[$key];
        }

        return;
    }

    public function __set($key, $value)
    {
        if (isset($this->fields[$key])) {
            $this->data[$key] = $value;
        } else {
            $this->extras[$key] = $value;
        }
    }
}
