<?php

namespace HasOffers\Model;

class VatRate extends AModel
{
    protected $fields = [
        'code' => 'string',
        'created' => 'DateTime',
        'id' => 'integer',
        'modified' => 'DateTime',
        'name' => 'string',
        'rate' => 'decimal',
    ];
}
