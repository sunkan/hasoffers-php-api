<?php

namespace HasOffers\Model;

class AdvertiserUser extends AModel
{
    protected $fields = [
        'access' => 'array',
        'advertiser_id' => 'integer',
        'cell_phone' => 'string',
        'email' => 'string',
        'first_name' => 'string',
        'id' => 'integer',
        'join_date' => 'DateTime',
        'last_login' => 'DateTime',
        'last_name' => 'string',
        'modified' => 'DateTime',
        'password' => 'string',
        'password_confirmation' => 'string',
        'permissions' => 'integer',
        'phone' => 'string',
        'status' => ['active', 'blocked', 'deleted'],
        'title' => 'string',
    ];
}
