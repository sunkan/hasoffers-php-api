<?php

namespace HasOffers\Model;

class Hostname extends AModel
{
    protected $fields = [
        'brand_id' => 'integer',
        'domain' => 'string',
        'has_ssl' => 'boolean',
        'id' => 'integer',
        'jump_redirect' => 'string',
        'status' => ['active', 'deleted'],
        'type' => ['jump'],
    ];
}
