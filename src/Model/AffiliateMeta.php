<?php

namespace HasOffers\Model;

class AffiliateMeta extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'ssn_tax' => 'string',
    ];
}
