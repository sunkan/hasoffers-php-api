<?php

namespace HasOffers\Model;

class OfferFile extends AModel
{
    protected $fields = [
        'account_id' => 'integer',
        'code' => 'string',
        'created' => 'DateTime',
        'display' => 'string',
        'filename' => 'string',
        'flash_vars' => 'string',
        'height' => 'integer',
        'id' => 'integer',
        'interface' => ['network', 'affiliate', 'advertiser'],
        'modified' => 'DateTime',
        'offer_id' => 'integer',
        'size' => 'integer',
        'status' => ['active', 'pending', 'deleted'],
        'thumbnail' => 'string',
        'type' => ['file', 'image banner', 'flash banner', 'email creative', 'offer thumbnail', 'text ad', 'html ad', 'hidden', 'xml feed'],
        'url' => 'string',
        'width' => 'integer',
    ];
}
