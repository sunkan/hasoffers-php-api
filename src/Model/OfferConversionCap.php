<?php

namespace HasOffers\Model;

class OfferConversionCap extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'conversion_cap' => 'integer',
        'id' => 'integer',
        'modified' => 'DateTime',
        'monthly_conversion_cap' => 'integer',
        'monthly_payout_cap' => 'decimal',
        'monthly_revenue_cap' => 'decimal',
        'offer_id' => 'integer',
        'payout_cap' => 'decimal',
        'revenue_cap' => 'decimal',
    ];
}
