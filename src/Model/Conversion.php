<?php

namespace HasOffers\Model;

class Conversion extends AModel
{
    protected $fields = [
        'ad_campaign_creative_id' => 'integer',
        'ad_campaign_id' => 'integer',
        'ad_id' => 'string',
        'advertiser_id' => 'integer',
        'advertiser_info' => 'string',
        'advertiser_manager_id' => 'integer',
        'affiliate_id' => 'integer',
        'affiliate_info1' => 'string',
        'affiliate_info2' => 'string',
        'affiliate_info3' => 'string',
        'affiliate_info4' => 'string',
        'affiliate_info5' => 'string',
        'affiliate_manager_id' => 'integer',
        'browser_id' => 'integer',
        'country_code' => 'string',
        'creative_url_id' => 'integer',
        'currency' => 'string',
        'customer_id' => 'integer',
        'datetime' => 'DateTime',
        'goal_id' => 'integer',
        'id' => 'integer',
        'internal_ad_id' => 'integer',
        'ip' => 'string',
        'is_adjustment' => 'boolean',
        'offer_file_id' => 'integer',
        'offer_id' => 'integer',
        'payout' => 'deciaml',
        'payout_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both', 'cpc', 'cpm'],
        'pixel_refer' => 'string',
        'refer' => 'string',
        'revenue' => 'deciaml',
        'revenue_type' => ['cpa_flat', 'cpa_percentage', 'cpa_both', 'cpc', 'cpm'],
        'sale_amount' => 'deciaml',
        'session_datetime' => 'DateTime',
        'session_ip' => 'string',
        'source' => 'string',
        'status' => ['approved', 'pending', 'rejected'],
        'status_code' => 'integer',
        'user_agent' => 'string',
    ];
}
