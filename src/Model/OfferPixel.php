<?php

namespace HasOffers\Model;

class OfferPixel extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'code' => 'string',
        'goal_id' => 'integer',
        'id' => 'integer',
        'modified' => 'DateTime',
        'offer_id' => 'integer',
        'status' => ['active', 'pending', 'rejected', 'deleted'],
        'type' => ['url', 'code', 'image'],
    ];
}
