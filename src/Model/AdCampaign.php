<?php

namespace HasOffers\Model;

class AdCampaign extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'name' => 'string',
        'width' => 'integer',
        'height' => 'integer',
        'account_id' => 'integer',
        'custom_weights' => 'boolean',
        'affiliate_access' => 'boolean',
        'type' => ['text', 'banner', 'link'],
        'modified' => 'DateTime',
        'interface' => ['affiliate', 'network'],
        'optimization' => 'boolean',
        'status' => ['active', 'paused', 'deleted'],
        'optimization_field' => ['ctr', 'rpm', 'cr', 'rpc', 'profit'],
    ];
}
