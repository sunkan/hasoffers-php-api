<?php

namespace HasOffers\Model;

class AffiliateAccountBalance extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'balance' => 'decimal',
    ];
}
