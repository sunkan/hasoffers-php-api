<?php

namespace HasOffers\Model;

class OfferCategory extends AModel
{
    protected $fields = [
        'date_created' => 'DateTime',
        'date_updated' => 'DateTime',
        'id' => 'integer',
        'name' => 'string',
        'status' => ['active', 'deleted'],
    ];
}
