<?php

namespace HasOffers\Model;

class Advertiser extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'account_manager_id' => 'integer',
        'address1' => 'string',
        'address2' => 'string',
        'city' => 'string',
        'company' => 'string',
        'conversion_security_token' => 'string',
        'country' => 'string',
        'fax' => 'string',
        'modified' => 'DateTime',
        'date_added' => 'DateTime',
        'phone' => 'string',
        'ref_id' => 'string',
        'region' => 'string',
        'signup_ip' => 'string',
        'status' => ['pending', 'active', 'blocked', 'deleted', 'rejected'],
        'tmp_token' => 'string',
        'zipcode' => 'string',
    ];
}
