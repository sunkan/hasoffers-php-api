<?php

namespace HasOffers\Model;

class PaymentMethodCheck extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'address1' => 'string',
        'address2' => 'string',
        'city' => 'string',
        'country' => 'string',
        'payable_to' => 'string',
        'region' => 'string',
        'zipcode' => 'string',
        'is_individual' => 'boolean',
    ];
}
