<?php

namespace HasOffers\Model;

class AffiliateTier extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'is_default' => 'boolean',
        'level' => 'integer',
        'name' => 'string',
        'status' => ['active', 'deleted'],
    ];
}
