<?php

namespace HasOffers\Model;

class SavedReports extends AModel
{
    protected $fields = [
        'data' => 'object',
        'hash' => 'string',
        'id' => 'integer',
        'interface' => ['affiliate', 'network', 'advertiser'],
        'name' => 'string',
        'type' => ['stats', 'conversions'],
        'user_id' => 'integer',
    ];
}
