<?php

namespace HasOffers\Model;

class AffiliateTierPayout extends AModel
{
    protected $fields = [
        'affiliate_tier_id' => 'integer',
        'goal_id' => 'integer',
        'id' => 'integer',
        'modified' => 'DateTime',
        'offer_id' => 'integer',
        'payout' => 'decimal',
        'percent_payout' => 'decimal',
    ];
}
