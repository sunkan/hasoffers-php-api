<?php

namespace HasOffers\Model;

class AccountPreference extends AModel
{
    protected $fields = [
        'id' => 'integer',
        'account_id' => 'integer',
        'name' => 'string',
        'value' => 'string',
        'type' => ['affiliate', 'advertiser', 'employee'],
    ];
}
