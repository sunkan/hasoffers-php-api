<?php

namespace HasOffers\Model;

class DataAudit extends AModel
{
    protected $fields = [
        'action_time_utc' => 'DateTime',
        'detected_ip' => 'string',
        'id' => 'integer',
        'new_data' => 'object',
        'old_data' => 'object',
        'operation' => ['insert', 'update', 'delete'],
        'source' => 'string',
        'table_name' => 'string',
        'table_pkey_name' => 'string',
        'table_row_pkey_val' => 'string',
        'user_id' => 'integer',
        'user_ip' => 'string',
        'user_type' => ['AdvertiserUser', 'AffiliateUser', 'Employee'],
    ];
}
