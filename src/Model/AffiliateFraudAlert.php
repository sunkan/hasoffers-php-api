<?php

namespace HasOffers\Model;

class AffiliateFraudAlert extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'date_updated' => 'DateTime',
        'id' => 'integer',
        'is_active' => 'boolean',
        'is_enabled' => 'boolean',
        'notes' => 'string',
        'preference_name' => [
            'affiliate_fraud_profile_criteria_po_box',
            'affiliate_fraud_profile_criteria_no_address_number',
            'affiliate_fraud_profile_criteria_address_length',
            'affiliate_fraud_profile_criteria_region_zip_match',
            'affiliate_fraud_profile_criteria_email_domain',
            'affiliate_fraud_profile_criteria_free_email',
            'affiliate_fraud_profile_criteria_phone_country_match',
            'affiliate_fraud_profile_criteria_phone_800',
            'affiliate_fraud_profile_criteria_ip_country_match',
            'affiliate_fraud_profile_criteria_individuals_check',
            'affiliate_fraud_profile_criteria_duplicate_signup',
            'affiliate_fraud_profile_criteria_account_changes',
        ],
    ];
}
