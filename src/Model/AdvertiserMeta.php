<?php

namespace HasOffers\Model;

class AdvertiserMeta extends AModel
{
    protected $fields = [
        'advertiser_id' => 'integer',
        'default_vat_id' => 'integer',
        'ssn_tax' => 'string',
    ];
}
