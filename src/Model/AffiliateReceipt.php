<?php

namespace HasOffers\Model;

class AffiliateReceipt extends AModel
{
    protected $fields = [
        'affiliate_id' => 'integer',
        'amount' => 'decimal',
        'currency' => 'string',
        'date' => 'Date',
        'datetime' => 'DateTime',
        'id' => 'integer',
        'memo' => 'string',
        'method' => ['wire', 'check', 'paypal', 'other', 'direct_deposit', 'payoneer', 'payquicker'],
        'notes' => 'string',
        'payment_details' => 'string',
        'payment_info' => 'object',
        'status' => [],
        'token' => 'string',
        'transaction_id' => 'string',
    ];
}
