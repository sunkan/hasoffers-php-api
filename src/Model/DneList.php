<?php

namespace HasOffers\Model;

class DneList extends AModel
{
    protected $fields = [
        'count' => 'integer',
        'file_hash' => 'string',
        'id' => 'integer',
        'is_external' => 'boolean',
        'modified' => 'DateTime',
        'name' => 'string',
        'provider' => ['UnsubCentral'],
        'ref_id' => 'string',
        'status' => ['enabled', 'disabled'],
        'unsubscribe_link' => 'string',
        'url' => 'string',
    ];
}
