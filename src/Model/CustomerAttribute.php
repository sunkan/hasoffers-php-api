<?php

namespace HasOffers\Model;

class CustomerAttribute extends AModel
{
    protected $fields = [
        'customer_id' => 'Integer',
        'id' => 'Integer',
        'modified' => 'DateTime',
        'name' => 'string',
        'value' => 'string',
    ];
}
