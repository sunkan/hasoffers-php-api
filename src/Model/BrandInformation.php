<?php

namespace HasOffers\Model;

class BrandInformation extends AModel
{
    protected $fields = [
        'company_address1' => 'string',
        'company_address2' => 'string',
        'company_city' => 'string',
        'company_country' => 'string',
        'company_name' => 'string',
        'company_phone' => 'string',
        'company_region' => 'string',
        'company_zipcode' => 'string',
    ];
}
