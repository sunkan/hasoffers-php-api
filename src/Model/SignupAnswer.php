<?php

namespace HasOffers\Model;

class SignupAnswer extends AModel
{
    protected $fields = [
        'answer' => 'string',
        'id' => 'integer',
        'modified' => 'DateTime',
        'question_id' => 'integer',
        'ref_id' => 'integer',
        'responder_id' => 'integer',
        'responder_type' => ['affiliate', 'advertiser'],
    ];
}
