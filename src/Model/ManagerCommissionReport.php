<?php

namespace HasOffers\Model;

class ManagerCommissionReport extends AModel
{
    protected $fields = [
        'AffiliateManager.full_name' => 'string',
        'Stat.affiliate_manager_id' => 'integer',
        'Stat.commission_amount' => 'decimal',
        'Stat.commission_base' => 'decimal',
        'Stat.commission_field' => ['leads', 'payout', 'revenue', 'profit'],
        'Stat.commission_rate' => 'decimal',
        'Stat.commission_type' => ['flat', 'percentage'],
        'Stat.date' => 'Date',
    ];
}
