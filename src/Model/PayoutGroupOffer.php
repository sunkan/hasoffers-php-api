<?php

namespace HasOffers\Model;

class PayoutGroupOffer extends AModel
{
    protected $fields = [
        'cashflow_group_id' => 'array',
        'offer_id' => 'integer',
        'id' => 'integer',
        'percent' => 'decimal',
        'rate' => 'decimal',
    ];
}
