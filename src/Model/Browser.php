<?php

namespace HasOffers\Model;

class Browser extends AModel
{
    protected $fields = [
        'bit_pos' => 'integer',
        'class' => 'string',
        'group' => 'string',
        'group_id' => 'integer',
        'id' => 'integer',
        'name' => 'string',
    ];
}
