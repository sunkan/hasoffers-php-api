<?php

namespace HasOffers;

use Guzzle\Http\Client as HttpClient;
use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Exception\ParseException;
use Guzzle\Http\Exception\ClientErrorResponseException;

class Client
{
    public static $cache = true;
    protected $http_client;
    protected $request_count = 0;
    protected $services = [
        'affiliate' => [
            'AdManager',
            'Affiliate',
            'AffiliateBilling',
            'AffiliateUser',
            'Alert',
            'ApiKey',
            'Application',
            'BrandDesign',
            'BrandInformation',
            'Goal',
            'Notification',
            'Offer',
            'OfferFile',
            'OfferPixel',
            'OfferTargeting',
            'OfferUrl',
            'Report',
            'RingRevenue',
            'SavedReports',
            'UserPreferences'
        ],
        'brand' => [
            'AdManager',
            'Advertiser',
            'AdvertiserBilling',
            'AdvertiserUser',
            'Affiliate',
            'AffiliateBilling',
            'AffiliateUser',
            'Alert',
            'ApiKey',
            'Application',
            'Auditor',
            'Authentication',
            'CashflowGroup',
            'Conversion',
            'ConversionMeta',
            'CustomerList',
            'CustomMenuLink',
            'CustomPage',
            'DneList',
            'DownloadReport',
            'Employee',
            'Environment',
            'Goal',
            'Notification',
            'Offer',
            'OfferDisabledLink',
            'OfferFile',
            'OfferPixel',
            'OfferTargeting',
            'OfferUrl',
            'OfferWhitelist',
            'Preference',
            'RawLog',
            'Report',
            'RingRevenue',
            'SavedReports',
            'TinyUrl',
            'VatRate',
        ]
    ];

    public $jsonResponse;
    public $error;

    public function __construct($url, $network_id, HttpClient $http_client)
    {
        $this->http_client = $http_client;
        $this->api_url = $url;
        $this->network_id = $network_id;
    }

    public function makeRequest($requestMethod, $target, $method, $data)
    {
        $data['Target'] = $target;
        $data['Method'] = $method;
        $data['NetworkId'] = $this->network_id;

        $url = $this->api_url . '/Apiv3/json';

        if (strtolower($requestMethod) == 'get') {
            $url .= '?' . http_build_query($data);
        } else {
            $postBody = $data;
        }
        $this->jsonResponse = false;
        $this->error = false;

        if ($this->memcached && self::$cache) {
            $cacheKey = 'v6:ho-api-' . md5(serialize([$requestMethod, $url, $postBody ?: '']));
            $this->jsonResponse = $this->memcached->get($cacheKey);
        }
        if (!$this->jsonResponse) {
            $request = $this->http_client->createRequest($requestMethod, $url, null, $postBody ?: null);
            try {
                $this->response = $this->http_client->send($request);
            } catch (ClientErrorResponseException $cere) {
                $this->error = true;
                $this->response = $cere->getResponse();
            }
            try {
                $this->jsonResponse = $this->response->json();
            } catch (\Exception $re) {
                throw new InvalidResponseException('Invalid json response');
            }
            if ($this->memcached && !$this->error && $this->jsonResponse['response']['status'] != -1) {
                $this->memcached->set($cacheKey, $this->jsonResponse, 300 + $this->request_count);
            }
            $this->request_count++;
        } else {
//            var_dump('cache');
        }
        return $this->jsonResponse;

    }

    protected $memcached = false;

    public function setMemcached($mem)
    {
        $this->memcached = $mem;
    }

    protected $options = [];

    public function setBrandOptions(array $opts)
    {
        $this->options['brand'] = $opts;
    }

    public function setAffiliateOptions(array $opts)
    {
        $this->options['affiliate'] = $opts;
    }

    protected $servicesCache = ['Affiliate' => [], 'Brand' => []];

    public function getService($name, array $options = [])
    {
        if (strpos($name, '.') === false) {
            $section = 'brand';
            $target = $name;
        } else {
            list($section, $target) = explode('.', $name, 2);
        }
        $section = strtolower($section);

        $options = array_merge($options, $this->options[$section] ?: []);

        if (isset($this->services[$section]) && in_array($target, $this->services[$section])) {
            if (!$this->servicesCache[$section][$target]) {
                $class = '\\HasOffers\\' . ucfirst($section) . '\\' . $target;
                $this->servicesCache[$section][$target] = new $class($this, $options);
                $this->servicesCache[$section][$target]->setMemcached($this->memcached);
            }
            return $this->servicesCache[$section][$target];
        }
    }
}
