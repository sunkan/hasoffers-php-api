<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class OfferTargeting extends AService
{
    public function getRuleTargetingForOffer($offer_id)
    {
        if (!is_int($offer_id) || $offer_id < 1) {
            throw new InvalidArgumentException('offer_id must be an integer and be positive');
        }
        $payload = [];
        $payload['offer_id'] = $offer_id;

        $response = $this->makeRequest(__METHOD__, $payload);

        return new Model\TargetRuleOffer($response['data']);
    }
}
