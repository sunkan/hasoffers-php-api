<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;
use InvalidArgumentException;

class UserPreferences extends AService
{
    public function setValue($name, $value)
    {
        if (!in_array($name, ['disable_mail_room_email', 'enforce_session_ip', 'language', 'timezone_id'])) {
            throw new InvalidArgumentException('invlaid value for name');
        }
        $payload = [];
        $payload['name'] = $name;
        $payload['value'] = $value;

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
