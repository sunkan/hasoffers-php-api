<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;
use InvalidArgumentException;

class RingRevenue extends AService
{
    public function createAffiliateLoginUrl($offer_id, $url_path)
    {
        if (!is_int($offer_id) || $offer_id < 1) {
            throw new InvalidArgumentException('offer_id must be an integer and be positive');
        }
        $payload = [];
        $payload['offer_id'] = $offer_id;
        $payload['url_path'] = $url_path;

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
