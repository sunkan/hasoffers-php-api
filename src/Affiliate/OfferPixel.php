<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class OfferPixel extends AService
{
    public function create($data)
    {
        if (!($data instanceof Model\OfferPixel)) {
            $data = new Model\OfferPixel($data);
        }
        $payload = [];
        $payload['data'];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findAll(array $filters = [], array $sort = [], $limit = false, $page = false, array $fields = [], array $contain = [])
    {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limt && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = $page;
        }
        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }
        if ($contain && count($contain)) {
            $payload['contain'] = $contain;
        }

        $response = $this->makeRequest(__METHOD__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\OfferPixel($row);
        }

        return $rows;
    }
    public function getAllowedTypes($offer_id)
    {
        if (!is_int($offer_id) || $offer_id < 1) {
            throw new InvalidArgumentException('offer_id must be an integer and be positive');
        }
        $payload = [];
        $payload['offer_id'] = $offer_id;

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function updateField($id, $field, $value)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!in_array($field, ['code', 'goal_id', 'offer_id', 'type'])) {
            throw new InvalidArgumentException('invalid value for field');
        }
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
