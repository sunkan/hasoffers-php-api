<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;

class Affiliate extends AService
{
    public function findById()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getAccountBalance()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getAccountManager()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getPayoneerSignupUrl()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getReferralCommission()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function update()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function updatePaymentMethodCheck()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function updatePaymentMethodDirectDeposit()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function updatePaymentMethodOther()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function updatePaymentMethodPaypal()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function updatePaymentMethodPayQuicker()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function updatePaymentMethodWire()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
