<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class OfferFile extends AService
{
    /**
     * @todo upload
     */
    public function create()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findAll(array $filters = [], array $sort = [], $limit = false, $page = false, array $fields = [])
    {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limt && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = $page;
        }
        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__METHOD__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\OfferFile($row);
        }

        return $rows;
    }
    public function findById($id, array $fields = [])
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        if (count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'] ? new Model / OfferFile($response['data']) : null;
    }
    public function getCreativeCode($id, $tracking_url, $impression_pixel, $etracking_url, $offer_id, $source, $aff_sub, $aff_sub2, $aff_sub3, $aff_sub4, $aff_sub5)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        if (!is_int($offer_id) || $offer_id < 1) {
            throw new InvalidArgumentException('offer_id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;
        $payload['tracking_url'] = $tracking_url;
        $payload['impression_pixel'] = $impression_pixel;
        $payload['etracking_url'] = $etracking_url;
        $payload['offer_id'] = $offer_id;
        $payload['source'] = $source;
        $payload['aff_sub'] = $aff_sub;
        $payload['aff_sub2'] = $aff_sub2;
        $payload['aff_sub3'] = $aff_sub3;
        $payload['aff_sub4'] = $aff_sub4;
        $payload['aff_sub5'] = $aff_sub5;

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
