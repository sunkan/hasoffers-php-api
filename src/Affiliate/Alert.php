<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;

class Alert extends AService
{
    public function dismissAffiliateUserAlert()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findById()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getAffiliateUserAlerts()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
