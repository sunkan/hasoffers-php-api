<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class OfferUrl extends AService
{
    public function findAll(array $filters = [], array $sort = [], $limit = false, $page = false, array $fields = [])
    {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limt && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = $page;
        }
        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__METHOD__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\OfferUrl($row);
        }

        return $rows;
    }
}
