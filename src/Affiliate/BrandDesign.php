<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;

class BrandDesign extends AService
{
    public function getTermsAndConditions()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
