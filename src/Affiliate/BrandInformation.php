<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;

class BrandInformation extends AService
{
    public function getBrandInformation()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
