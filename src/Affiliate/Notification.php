<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;

class Notification extends AService
{
    public function clearUserSubscriptions()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getUserSubscriptions(array $filters = [])
    {
        $payload = [];
        if (count($filters)) {
            $payload['filters'] = $filters;
        }

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function replaceUserSubscriptions($scenario_ids)
    {
        $payload = [];
        $payload['scenario_ids'] = $scenario_ids;

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
