<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;

class Offer extends AService
{
    public function acceptOfferTermsAndConditions()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findAll()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findAllFeaturedOfferIds()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findByCreativeType()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findById()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function findMyOffers()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getApprovalQuestions()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getCategories()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getPayoutDetails()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getPixels()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getTargetCountries()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function getThumbnail()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
    public function requestOfferAccess()
    {
        $payload = [];

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }
}
