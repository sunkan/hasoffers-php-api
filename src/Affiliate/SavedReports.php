<?php

namespace HasOffers\Affiliate;

use HasOffers\AService;
use HasOffers\Model;
use InvalidArgumentException;

class SavedReports extends AService
{
    public function delete($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__METHOD__, $payload);

        return $response['data'];
    }

    public function findAll(array $filters = [], array $sort = [], $limit = false, $page = false, array $fields = [])
    {
        if ($page && (!is_int($page) || $page < 1)) {
            throw new InvalidArgumentException('Page must be an integer and be positive');
        }
        if ($limt && (!is_int($limit) || $limit < 1)) {
            throw new InvalidArgumentException('Limit must be an integer and be positive');
        }
        $payload = [];
        if ($page) {
            $payload['page'] = $page;
        }
        if ($limit) {
            $payload['limit'] = $limit;
        }
        if ($filters && count($filters)) {
            $payload['filters'] = $filters;
        }
        if ($sort && count($sort)) {
            $payload['sort'] = $sort;
        }
        if ($fields && count($fields)) {
            $payload['fields'] = $fields;
        }

        $response = $this->makeRequest(__METHOD__, $payload);

        $rows = [];
        foreach ($response['data'] as $row) {
            $rows[] = new Model\SavedReports($row);
        }

        return $rows;
    }
    public function findById($id)
    {
        if (!is_int($id) || $id < 1) {
            throw new InvalidArgumentException('id must be an integer and be positive');
        }
        $payload = [];
        $payload['id'] = $id;

        $response = $this->makeRequest(__METHOD__, $payload);

        return new Model\SavedReports($response['data']);
    }
}
