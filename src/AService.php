<?php

namespace HasOffers;

use HasOffers\Model\AModel;
use DateTime;

abstract class AService
{
    protected $client;
    protected $options = [];
    protected $target;
    protected $memcached;

    private function get_real_class($obj)
    {
        $classname = get_class($obj);

        if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
            $classname = $matches[1];
        }

        return $classname;
    }

    public function setMemcached($mem)
    {
        $this->memcached = $mem;
    }

    public function __construct(Client $client, $options)
    {
        $this->client = $client;
        $this->options = $options;
    }

    protected function makeRequest($method, $data, $requestMethod = false)
    {
        $target = $this->target;
        if (!$target) {
            $target = $this->get_real_class($this);
        }
        if (!$requestMethod) {
            $requestMethod = 'GET';
            if (stripos($method, 'find') === false) {
                $requestMethod = 'POST';
            }
        }

        $payload = [];
        foreach ($data as $key => $value) {
            if ($value instanceof AModel) {
                $payload[$key] = $value->getData();
            } elseif (is_bool($value)) {
                $payload[$key] = (int) $value;
            } else {
                $payload[$key] = $value;
            }
        }
        $result = $this->client->makeRequest($requestMethod, $target, $method, $payload);

        $response = $result['response'];
        if ($response['status'] != 1) {
            $e = new \Exception($response['errors'][0]['publicMessage'], 1000000000);
            $e->response = $response;
            $e->payload = [
                $requestMethod,
                $target,
                $method,
                $payload,
            ];
            $e->request = $result['request'];
            throw $e;
        }

        return $response['data'];
    }

    protected function _getDate($date)
    {
        if (!($date instanceof DateTime)) {
            $date = new DateTime($date);
        }

        return $date;
    }
}
